import React, { Component } from 'react';
import './login.css';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

export class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            auth_token: '',
            id: '',
            password: '',
            toOffersPage: false

        }
        this.get_bearer = this.get_bearer.bind(this);
        this.handleUsername = this.handleUsername.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleUsername = (e) => {
        this.setState({ id: e.target.value })
    }
    handlePassword = (e) => {
        this.setState({ password: e.target.value })
    }
    get_bearer = async () => {

        const token = Buffer.from(`${this.state.id}:${this.state.password}`, 'utf8').toString('base64')
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        axios.get(`${proxyurl}https://teklifimgelsin.herokuapp.com/api/login`,
            {
                headers: {
                    'Authorization': `Basic ${token}`
                }
            }
        )
            .then((response) => {
                console.log('response' + response.data.token);
                this.setState({ auth_token: response.data.token });
                window.localStorage.setItem("token", response.data.token);
                window.localStorage.setItem("auth",true);
                this.setState({toOffersPage:true})
            }, (error) => {
                console.log(error);
            });

    }
    handleLogin = async (e) => {
        e.preventDefault();
        console.log("submitted")
        await this.get_bearer();
        
        console.log(this.state.toOffersPage)


    }
    componentDidMount() {

        console.log("mount")


    }
    render() {
        if (window.localStorage.getItem("auth")){
            return <Redirect to={{pathname:"/offers"}}/>;
        }
         
        return (
            <div className='login_box'>
                <form method='POST' onSubmit={this.handleLogin}>
                    <div className='form-group'>
                        <label htmlFor="uname"><b>Username</b></label>
                        <input type="text" placeholder="Enter Username" name="uname" required onChange={this.handleUsername} />
                    </div>
                    <div className='form-group'>
                        <label htmlFor="psw"><b>Password</b></label>
                        <input type="password" placeholder="Enter Password" name="psw" required onChange={this.handlePassword} />
                    </div>
                    <button type="submit">Sign in</button>
                </form>
            </div>
        )
    }
}

export default Login
