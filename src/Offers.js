import React, { Component } from 'react';
import { Col, Row, Card, Pagination } from 'antd';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

const { Meta } = Card;

export default class Offers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            meta: [],
            links: [],
            currentPage: 1,
        }
        this.onChange = this.onChange.bind(this)
    }

    onChange = async (e) => {
        this.setState({ currentPage: e })
        this.get_data();
        console.log("data has come")
    }

    get_data = async () => {
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        try {
            const response = await axios.get(`${proxyurl}https://teklifimgelsin.herokuapp.com/api/offers?page=${this.state.currentPage}&per_page=3`,
                {
                    headers: {
                        'Authorization': `Bearer ${window.localStorage.getItem("token")}`
                    }
                }
            )
            await this.setState({items:response.data.items,meta:response.data._meta,links:response.data._links })
            return response;
        }   catch (e){
            console.log(e);
        }
    }

    componentDidMount() {
        this.get_data();
        
        console.log("mounted")
    }
    render() {
        return (
            <>
                <Row justify="space-around" gutter={16}>
                    {this.state.items.map((item, index) =>
                        <Col key={index}>
                            <Card hoverable style={{ width: 240 }} cover={<img alt="example" src="https://cdnuploads.aa.com.tr/uploads/sirkethaberleri/Contents/2018/11/30/thumbs_b_c_eee85060d5e32db6d1bb1e2da1ca285e.jpg" />}>
                                <Meta title="Europe Street beat" />
                                <p> Amount : {item.amount}</p>
                                <p> Type : {item.transaction_type}</p>
                                <p> Interest : {item.interest}</p>
                                <p> Vade : {item.vade}</p>
                            </Card>
                        </Col>
                    )}
                </Row>
                <Pagination onChange={this.onChange} style={{ padding: "16px" }} total={this.state.meta.total_items}
                    current={this.state.currentPage}
                    defaultPageSize={3}
                    showQuickJumper
                    showSizeChanger />
            </>
        )
    }
}
