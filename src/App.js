import React, { Component } from 'react';
import './App.css';
import Login from './Login';
import OffersPage from './OffersPage';
import './login.css';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import ProtectedRoute from './ProtectedRoute';



export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <>
        <Router>

          <Route path="/login" component={Login} />
          <ProtectedRoute exact path="/offers" component={OffersPage} />
          <Route path="/offers/:id">
            {/*<OfferDetail/>*/}
          </Route>
        </Router>

      </>
    );
  }
}

