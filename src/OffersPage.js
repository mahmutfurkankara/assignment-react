import React, { Component } from 'react';
import Offers from './Offers';
import { Layout, Menu, Breadcrumb } from 'antd';
import 'antd/dist/antd.css';
import './index.css';
import {Redirect} from 'react-router-dom'

const { Header, Content, Footer } = Layout;
export default class OffersPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            redirectToLogin: false,
        }
        this.logoutHandler = this.logoutHandler.bind(this);
    }

    logoutHandler =()=>{
        window.localStorage.removeItem("auth")
        window.localStorage.removeItem("token")
        this.setState({redirectToLogin:true})
    }
    render() {
        if (this.state.redirectToLogin) return <Redirect to="/login" />;
        return (
            <>
                <Layout style={{ height: "100vh" }} className="layout">
                    <Header>
                        <Menu style={{ float: 'right' }} theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                            <Menu.Item key="1" onClick={this.logoutHandler}>Logout</Menu.Item>
                        </Menu>
                    </Header>
                    <Content style={{ margin: 'auto' }}>
                        <Breadcrumb style={{ margin: '16px 0' }}>
                            <Breadcrumb.Item>Home</Breadcrumb.Item>
                            <Breadcrumb.Item>List</Breadcrumb.Item>
                            <Breadcrumb.Item>App</Breadcrumb.Item>
                        </Breadcrumb>
                        <div className="site-layout-content">
                            <Offers />
                        </div>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
                </Layout>
            </>
        )
    }
}
